//usr/bin/env go run "${0}" "${@}"; exit ${?};
//go:generate go vet $GOFILE
//go:generate go fmt $GOFILE

//	©2019 Paul John King (paul_john_king@web.de).  All rights reserved.
//
//	This program is free software: you can redistribute it and/or modify it
//	under the terms of the GNU General Public License, version 3 as published
//	by the Free Software Foundation.
//
//	This program is distributed in the hope that it will be useful, but WITHOUT
//	ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//	more details.
//
//	You should have received a copy of the GNU General Public License along
//	with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

type (
	//	Project types
	//	-------------

	Project interface {
		NewGoal(name string) (goal Goal)
		GetGoal(name string) (goal Goal)
		Execute()
	}

	_Project struct {
		goals map[string]Goal
	}

	//	Goal types
	//	----------

	Goal interface {
		AddTask(task Task) (goal Goal)
		Execute()
	}

	_Goal struct {
		tasks []Task
	}

	//	Task types
	//	----------

	Task interface {
		Execute()
	}

	_BuildGoDists struct {
		dists      []*goDist
		sourceFile string
		targetDir  string
	}

	_RemovePaths struct {
		paths []string
	}

	_RunCommand struct {
		invocation string
		arguments  []string
		variables  []string
	}

	//	Utility types
	//	-------------

	goDist struct {
		os   string
		arch string
	}
)

//	Project constructor
//	-------------------

func NewProject() (project Project) {
	var (
		_project *_Project
	)

	_project = new(_Project)
	_project.goals = make(map[string]Goal)

	project = Project(_project)

	return
}

//	Project methods
//	---------------

func (this *_Project) NewGoal(name string) (goal Goal) {
	var (
		exists bool
		_goal  *_Goal
	)

	_, exists = this.goals[name]

	if exists {
		log.Fatalf("A goal with the name '%s' already exists.", name)
	}

	_goal = new(_Goal)
	_goal.tasks = []Task{}

	goal = Goal(_goal)
	this.goals[name] = goal

	return
}

func (this *_Project) GetGoal(name string) (goal Goal) {
	var (
		exists bool
	)

	goal, exists = this.goals[name]

	if !exists {
		log.Fatalf("No goal with the name '%s' exists.", name)
	}

	return
}

func (this *_Project) Execute() {
	for _, name := range os.Args[1:] {
		this.GetGoal(name).Execute()
	}

	return
}

//	Goal methods
//	------------

func (this *_Goal) AddTask(task Task) (goal Goal) {
	this.tasks = append(this.tasks, task)

	goal = Goal(this)

	return
}

func (this *_Goal) Execute() {
	for _, task := range this.tasks {
		task.Execute()
	}

	return
}

//	Task implementations
//	--------------------

func BuildGoDists(sourceFile string, targetDir string) (task Task) {
	var (
		err   error
		_task *_BuildGoDists
	)

	_task = new(_BuildGoDists)

	_task.dists, err = getGoDists()
	if err != nil {
		log.Fatal(err)
	}
	_task.sourceFile = sourceFile
	_task.targetDir = targetDir

	task = Task(_task)

	return
}

func (this *_BuildGoDists) Execute() {
	var (
		err        error
		sourceFile string
		path       string
		targetDir  string
	)

	sourceFile, err = filepath.Abs(this.sourceFile)
	if err != nil {
		log.Fatal(err)
	}

	for _, dist := range this.dists {
		path = filepath.Join(this.targetDir, dist.os, dist.arch)

		err = makeDir(0755, path)
		if err != nil {
			log.Fatal(err)
		}

		err = visitDir(
			path,
			func() {
				targetDir, err = os.Getwd()
				if err != nil {
					log.Fatal(err)
				}

				log.Printf("make '%s' in direectory '%s'", sourceFile, targetDir)

				return
			},
		)

	}

	return
}

func RemovePaths(paths ...string) (task Task) {
	var (
		_task *_RemovePaths
	)

	_task = new(_RemovePaths)
	_task.paths = paths

	task = Task(_task)

	return
}

func (this *_RemovePaths) Execute() {
	var (
		err error
	)

	for _, path := range this.paths {
		err = removePath(path)
		if err != nil {
			log.Fatal(err)
		}
	}

	return
}

func RunCommand(argv ...string) (task Task) {
	var (
		_task *_RunCommand
		argc  int
		index int
	)

	//	I create myself.

	_task = new(_RunCommand)

	//	I extract my environment variables assignments from the command line.

	_task.variables = os.Environ()

	argc = len(argv)
	for index = 0; index < argc; index++ {
		if strings.Index(argv[index], "=") >= 0 {
			_task.variables = append(_task.variables, argv[index])
		} else {
			break
		}
	}

	//	I extract my command invocation from the command line.

	if index == argc {
		log.Fatalf(
			"The command line '%s' contains no command invocation.",
			strings.Join(argv, "' '"),
		)
	}

	_task.invocation = argv[index]

	//	I extract my command arguments from the command line.

	_task.arguments = argv[index+1:]

	task = Task(_task)

	return
}

func (this *_RunCommand) Execute() {
	var (
		err error
	)

	//	I run an external command using my invocation, arguments and variables.

	err = runCommand(
		this.invocation,
		this.arguments,
		this.variables,
		os.Stdout,
		os.Stderr,
	)
	if err != nil {
		log.Fatal(err)
	}

	return
}

//	Utilities
//	---------

func makeDir(perm int, path string) (err error) {
	var (
		mode os.FileMode
	)

	mode = os.ModeDir | os.FileMode(perm)

	err = os.MkdirAll(path, mode)
	if err != nil {
		return
	}

	return
}

func removePath(path string) (err error) {
	err = os.RemoveAll(path)
	if err != nil {
		return
	}

	return
}

func runCommand(
	invocation string,
	arguments []string,
	variables []string,
	stdout io.Writer,
	stderr io.Writer,
) (err error) {
	var (
		command    *exec.Cmd
		channel    chan error
		stdoutPipe io.ReadCloser
		stderrPipe io.ReadCloser
	)

	//	I create a command.

	command = new(exec.Cmd)
	command = exec.Command(invocation, arguments...)
	command.Env = append(os.Environ(), variables...)

	//	I attach pipes to the command's stdout and stderr, and start go
	//	routines that try to copy its stdout and stderr from the attached pipes
	//	to my stdout and stderr.

	channel = make(chan error)

	stdoutPipe, err = command.StdoutPipe()
	if err != nil {
		return
	}
	go func() {
		var (
			err error
		)
		_, err = io.Copy(stdout, stdoutPipe)
		channel <- err

		return
	}()

	stderrPipe, err = command.StderrPipe()
	if err != nil {
		return
	}
	go func() {
		var (
			err error
		)

		_, err = io.Copy(stderr, stderrPipe)
		channel <- err

		return
	}()

	//	I start the command and wait for it to close its stdout and stderr
	//	pipes.

	err = command.Start()
	if err != nil {
		return
	}

	err = <-channel
	if err != nil {
		return
	}
	err = <-channel
	if err != nil {
		return
	}

	return
}

func getGoDists() (dists []*goDist, err error) {
	var (
		stdoutBuffer bytes.Buffer
		stderrBuffer bytes.Buffer
		stdout       string
		stderr       string
	)

	//	I try to run an external command that writes the Go dist list to its
	//	stdout, and copy the command's stdout to my stdout.

	err = runCommand(
		"go",
		[]string{"tool", "dist", "list"},
		[]string{},
		&stdoutBuffer,
		&stderrBuffer,
	)
	stdout = stdoutBuffer.String()
	stderr = stderrBuffer.String()
	if len(stderr) > 0 {
		log.Print(stderr)
	}
	if err != nil {
		return
	}

	//	I try to extract a Go dist list from my stdout.

	for _, line := range strings.Split(strings.TrimSpace(stdout), "\n") {
		var (
			components []string
			dist       *goDist
		)

		components = strings.Split(line, "/")
		if len(components) != 2 {
			err = errors.New(fmt.Sprintf(
				"line '%s' does not contain two '/' separated components.",
				line,
			))

			return
		}

		dist = new(goDist)
		dist.os = components[0]
		dist.arch = components[1]

		dists = append(dists, dist)
	}

	return
}

func visitDir(path string, function func()) (err error) {
	var (
		cwd string
	)

	cwd, err = os.Getwd()
	if err != nil {
		return
	}

	err = os.Chdir(path)
	if err != nil {
		return
	}

	function()

	err = os.Chdir(cwd)
	if err != nil {
		return
	}

	return
}

//	Main function
//	-------------

//	FIXME: Eventually, all of the code above must be moved away into a library,
//	and this main function will simply import the library and use it.

func main() {
	const (
		sourceFile = "dram.go"
		targetDir  = "public"
	)

	log.SetFlags(0)
	log.SetOutput(os.Stdout)

	project := NewProject()

	project.NewGoal("build").
		AddTask(BuildGoDists(
			sourceFile,
			targetDir,
		))

	project.NewGoal("clean").
		AddTask(RemovePaths(
			targetDir,
		))

	project.Execute()
}
