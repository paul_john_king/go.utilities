//usr/bin/env go run "${0}" "${@}"; exit $?;
//go:generate go vet $GOFILE
//go:generate go fmt $GOFILE

//	©2019 Paul John King (paul_john_king@web.de).  All rights reserved.
//
//	This program is free software: you can redistribute it and/or modify it
//	under the terms of the GNU General Public License, version 3 as published
//	by the Free Software Foundation.
//
//	This program is distributed in the hope that it will be useful, but WITHOUT
//	ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//	more details.
//
//	You should have received a copy of the GNU General Public License along
//	with this program.  If not, see <http://www.gnu.org/licenses/>.

//	TODO: Complete the help message.

//	TODO: Write godocs.

//	TODO: Write a Go-based build tool -- the user should only need Go to test
//	and build this project.

package main

import (
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"syscall"
)

var (
	version = "?"
)

func getFileName(path string) string {
	return path[strings.LastIndex(path, "/")+1:]
}

func printVersion() {
	log.Print(version)
}

func printHelp(invocation string) {
	log.Printf(
		`
Usage:  %s [--help] [--version] ... [-w|--workdir] «workdir» ....

TO  BE COMPLETED

Options:
      --help             TO BE COMPLETED
      --version          TO BE COMPLETED
  -w, --workdir string   TO BE COMPLETED
`,
		invocation,
	)
}

func main() {
	var (
		err        error
		docker     string
		cwd        string
		invocation string
		uid        string
		gid        string
		argc       int
		args       []string
	)

	log.SetFlags(0)
	log.SetOutput(os.Stdout)

	invocation = getFileName(os.Args[0])

	docker, err = exec.LookPath("docker")
	if err != nil {
		log.Fatalf("Cannot find 'docker' executable on path: %s", err)
	}
	cwd, err = os.Getwd()
	if err != nil {
		log.Fatalf("Cannot find current working directory: %s", err)
	}
	uid = strconv.Itoa(os.Getuid())
	gid = strconv.Itoa(os.Getgid())
	argc = len(os.Args)

	args = []string{
		docker,
		"run",
		"--rm",
		"--interactive",
		"--tty",
		"--user=" + uid + ":" + gid,
	}

	for i := 1; i < argc; i++ {
		a := os.Args[i]
		switch {
		case a == "--help",
			a == "--help=1",
			a == "--help=t",
			a == "--help=T",
			a == "--help=true",
			a == "--help=TRUE",
			a == "--help=True":
			printHelp(invocation)
			args = append(args, a)
		case a == "--help=0",
			a == "--help=f",
			a == "--help=F",
			a == "--help=false",
			a == "--help=FALSE",
			a == "--help=False":
			args = append(args, a)
		case strings.HasPrefix(a, "--help="):
			log.Fatalf("invalid argument \"%s\" for \"--help\" flag", strings.TrimPrefix(a, "--help="))

		case a == "--version",
			a == "--version=1",
			a == "--version=t",
			a == "--version=T",
			a == "--version=true",
			a == "--version=TRUE",
			a == "--version=True":
			printVersion()
			return
		case a == "--version=0",
			a == "--version=f",
			a == "--version=F",
			a == "--version=false",
			a == "--version=FALSE",
			a == "--version=False":
		case strings.HasPrefix(a, "--version="):
			log.Fatalf("invalid argument \"%s\" for \"--version\" flag", strings.TrimPrefix(a, "--version="))

		case a == "-w", a == "--workdir":
			args = append(args, a)
			if i+1 < argc {
				i++
				a = os.Args[i]
				args = append(args, a, "--volume="+cwd+":"+a)
			}
		case strings.HasPrefix(a, "-w/"):
			args = append(args, a, "--volume="+cwd+":"+strings.TrimPrefix(a, "-w"))
		case strings.HasPrefix(a, "-w="):
			args = append(args, a, "--volume="+cwd+":"+strings.TrimPrefix(a, "-w="))
		case strings.HasPrefix(a, "--workdir="):
			args = append(args, a, "--volume="+cwd+":"+strings.TrimPrefix(a, "--workdir="))

		default:
			args = append(args, a)
		}
	}

	err = syscall.Exec(docker, args, os.Environ())
	if err != nil {
		log.Fatalf("Cannot execute 'docker': %s", err)
	}

	return
}
