#!/bin/sh

#	TODO: Make installation optional.

#	TODO: Remove dependency on `revet`.

#	TODO: Build for multiple platforms, ready for deployment.

#	TODO: Convert `go install` step to unbuildable Go, and run it via
#	`go:generate`.

set -e ;
set -u ;

_main() {
	local _package ;

	for _package in "dram.go" ;
	do
		go generate "${_package}" ;
		go install -ldflags "-X main.version=$(revet)" "${_package}" ;
	done ;

	return 0 ;
}

_main "${@}" ;
